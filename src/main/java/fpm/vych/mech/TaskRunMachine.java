package fpm.vych.mech;

import fpm.vych.mech.tasks.impl.Task1;
import fpm.vych.mech.tasks.impl.Task2;
import fpm.vych.mech.tasks.impl.Task4;

/**
 * Created by Srw on 04.12.13.
 */
public class TaskRunMachine {
    private static TaskRunMachine instance;
    private TaskRunMachine(){}

    public static TaskRunMachine getInstance() {
        if (instance == null) {
            instance = new TaskRunMachine();
        }
        return instance;
    }

    public void runTask(int taskNumber) {
        printTaskNumber(taskNumber);
        switch (taskNumber) {
            case 1:
                (new Task1()).run();
                break;
            case 2:
                (new Task2()).run();
                break;
            case 4:
                System.err.println("Замени x и f(x) в методе run класса Task4 на требуемые по заданию!");
                (new Task4()).run();
                break;
            default:
                System.err.println("Wrong task number " + taskNumber);
        }
    }

    private void printTaskNumber(int taskNumber) {
        System.out.println("__________TASK" + taskNumber + "__________");
    }
}
