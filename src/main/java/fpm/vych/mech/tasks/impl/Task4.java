package fpm.vych.mech.tasks.impl;

import fpm.vych.mech.tasks.AbstractTask;

import java.util.Arrays;

/**
    Аппроксимировать функцию методом наименьших квадратов.
 */
public class Task4 extends AbstractTask {
    @Override
    public void run() {
        int []x = {0, 2, 3, 4};
        int []f = {1, 5, 7, 11};
        solve(x, f);
    }

    private void solve(int[] x, int[] f) {
        int []fi0 = calcFi0(x);
        int []fi1 = caclFi1(x);
        int []fi2 = calcFi2(x);

        System.out.println("(fi0,fi0)=" + scalarMultiplication(fi0, fi0));
        System.out.println("(fi0,fi1)=" + scalarMultiplication(fi0, fi1));
        System.out.println("(fi0,fi2)=" + scalarMultiplication(fi0, fi2));
        System.out.println("(fi1,fi2)=" + scalarMultiplication(fi1, fi2));
        System.out.println("(fi2,fi2)=" + scalarMultiplication(fi2, fi2));
        System.out.println("(fi0,f)=" + scalarMultiplication(fi0, f));
        System.out.println("(fi1,f)=" + scalarMultiplication(fi1, f));
        System.out.println("(fi2,f)=" + scalarMultiplication(fi2, f));
    }


    private int[] calcFi0(int []x) {
        int []fi0 = new int[x.length];
        Arrays.fill(fi0, 1);
        return fi0;
    }

    private int[] caclFi1(int []x) {
        int []fi1 = new int[x.length];
        System.arraycopy(x, 0, fi1, 0, x.length);
        return fi1;
    }

    private int[] calcFi2(int []x) {
        int []fi2 = new int[x.length];
        for(int i=0; i<x.length; ++i) {
            fi2[i] = x[i]*x[i];
        }
        return fi2;
    }

    private int scalarMultiplication(int []arg1, int []arg2) {
        int result = 0;
        for(int i=0; i<arg1.length; ++i) {
            result += (arg1[i]*arg2[i]);
        }
        return result;
    }

}
