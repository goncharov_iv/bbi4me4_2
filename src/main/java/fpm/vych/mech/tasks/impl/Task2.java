package fpm.vych.mech.tasks.impl;

/**
 * Определить узлы и погрешность интерполяции функции f, на отрезке [left, right] многочленом степени *degree*,
 * расположив узлы наилучшим образом
 */
import fpm.vych.mech.tasks.AbstractTask;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Task2 extends AbstractTask {

    private double left;
    private double right;
    private double maxFn;

    private int degree;

    public Task2() {
        getInputData(System.in);
    }

    private void getInputData(InputStream in) {
        System.out.println("Границы отрезка, количество узлов и максимум n+1 производной на отрезке:");
        Scanner scanner = new Scanner(in);
        scanner.useLocale(Locale.ENGLISH);
        left = scanner.nextDouble();
        right = scanner.nextDouble();
        degree = scanner.nextInt();
        maxFn = scanner.nextDouble();
    }

    public Task2(double left, double right, int degree, double maxFn) {
        this.left = left;
        this.right = right;
        this.degree = degree;
        this.maxFn = maxFn;
    }

    @Override
    public void run() {
        System.out.println("Корни многочлена Чебышева:");
        System.out.println(getChebyshevPolynomialRoots());
        System.out.println("Погрешность:");
        System.out.println(calculateError());
    }

    private List<Double> getChebyshevPolynomialRoots() {
        List<Double> coefficients = new ArrayList<>();
        for (int i = 0; i <= degree; ++i) {
            double cos = Math.cos((Math.PI + 2 * Math.PI * i) / (2 * (degree + 1)));
            coefficients.add(0.5d * (left + right) + 0.5d * (right - left) * cos);
        }
        return coefficients;
    }

    /**
     * (max( f(n+1) )/(n+1)!) * w(x) <= e , где w(x) = 2^(1-2*(n+1)) * (b-a)^(n+1)
     */
    public double calculateError() {
        double wx = Math.pow(2, 1 - 2*(degree + 1)) * Math.pow(right - left, degree + 1);
        return (maxFn / (double)factorial(degree + 1)) * wx;
    }

    private int factorial(int n) {
        return n == 0 ? 1 : n * factorial(n - 1);
    }
}
