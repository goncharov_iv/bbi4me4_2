package fpm.vych.mech.tasks;

public abstract class AbstractTask {
    public abstract void run();
}
